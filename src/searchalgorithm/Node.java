package searchalgorithm;

public class Node implements Comparable<Node> {

	protected State state;
	protected Node parent;
	protected Operator operator;
	protected int depth;
	protected int pathCost;
	
	private static final Node DEFAULT_PARENT = null;
	private static final Operator DEFAULT_OPERATOR = null;
	private static final int DEFAULT_DEPTH = 0;
	private static final int MAX_DEPTH = Integer.MAX_VALUE;
	private static final int DEFAULT_PATH_COST = 0;
	private static final int MAX_PATH_COST = Integer.MAX_VALUE;
	
	public Node(State initialState, int initialPathCost) {
		this.state = initialState;
		this.parent = DEFAULT_PARENT;
		this.operator = DEFAULT_OPERATOR;
		this.depth = DEFAULT_DEPTH;
		this.pathCost = initialPathCost;
	}
	
	protected Node(State state, Node parent, Operator operator, int pathCost) {
		this.state = state;
		this.parent = parent;
		this.operator = operator;
		this.depth = parent.depth + 1;
		this.pathCost = pathCost;
	}
	
	/**
	 * toString, compareTo, equals and hashCode
	 */
	@Override
	public String toString() {
		return "NODE";
	}
	
	@Override
	public boolean equals(Object o) {
		return false;
	}
	
	@Override
	public int hashCode() {
		return 0;
	}
	
	@Override
	public int compareTo(Node o) {
		return 0;
	}
	
	public State getState() {
		return state;
	}
	
	public Node getParent() {
		return parent;
	}
	
	public Operator getOperator() {
		return operator;
	}
	
	public int getDepth() {
		return depth;
	}
	
	public int getPathCost() {
		return pathCost;
	}
}
