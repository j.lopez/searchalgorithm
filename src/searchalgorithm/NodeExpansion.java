package searchalgorithm;

import java.util.Collection;

public interface NodeExpansion {

	public Collection<? extends Node> expandNode(Node node);
	
}
