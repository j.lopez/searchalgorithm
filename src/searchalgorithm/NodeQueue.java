package searchalgorithm;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

class NodeQueue {

	private LinkedList<Node> queue;
	
	NodeQueue() {
		this.queue = new LinkedList<>();
	}
	
	NodeQueue(NodeQueue queue) {
		this.queue = new LinkedList<>(queue.queue);
	}
	
	void addFront(Node n) {
		queue.addFirst(n);
	}
	
	void addBack(Node n) {
		queue.add(n);
	}
	
	void queueFront(Collection<? extends Node> collection) {
		queue.addAll(0, collection);
	}
	
	void queueBack(Collection<? extends Node> collection) {
		queue.addAll(collection);
	}
	
	Node getNode(int index) {
		return queue.get(index);
	}
	
	void removeNode(int index) {
		queue.remove(index);
	}
	
	Node peek() {
		return queue.peek();
	}
	
	Node pop() {
		return queue.pop();
	}
	
	boolean isEmpty() {
		return queue.isEmpty();
	}
	
	int size() {
		return queue.size();
	}

	void sort() {
		Collections.sort(queue);
	}

}
