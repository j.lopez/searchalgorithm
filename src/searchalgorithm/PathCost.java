package searchalgorithm;

public interface PathCost {

	public int calculateCost(Node parent, Node leaf);
	
}
