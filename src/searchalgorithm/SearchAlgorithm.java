package searchalgorithm;

import java.util.HashSet;
import java.util.Set;

public abstract class SearchAlgorithm {

	private int depthLimit;
	private NodeQueue nodeQueue;
	private Set<Integer> hashCodes;

	public static final int MIN_DEPTH_LIMIT = 1;
	public static final int MAX_DEPTH_LIMIT = Integer.MAX_VALUE;
	private static final int MAX_SEARCH_ITERATIONS = Integer.MAX_VALUE;

	public SearchAlgorithm(State initialState, int initialPathCost, int depthLimit) {
		this.depthLimit = (depthLimit < MIN_DEPTH_LIMIT) ? MIN_DEPTH_LIMIT : depthLimit;
		this.nodeQueue = new NodeQueue();
		this.hashCodes = new HashSet<>();

		Node rootNode = new Node(initialState, initialPathCost);

		this.nodeQueue.addBack(rootNode);
		this.hashCodes.add(rootNode.hashCode());
	}
}
