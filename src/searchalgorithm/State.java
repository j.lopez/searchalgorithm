package searchalgorithm;

public interface State {

	public boolean testGoal();
	
}
